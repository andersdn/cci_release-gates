# Circle CI Examples #

## Using production deployment gates ##

This repo is to show example config of a circleCI pipeline to show how a deployment can be staged for release with steps not triggered until manual approval takes place

## How do we do it?

By using a `type: approval` block in conjunction with a branch filter, we can ensure that the job only runs on the production branch and when the gate is unlocked. 

```
workflows:
  example-workflow:
    jobs:
      - run-job-a
      - do-production-deploy:
          requires:
            - run-job-a
# ^ Once other steps have completed            
          filters:
            branches:
              only: 'production'            
# ^ IF we are on the production branch                          
          type: approval
# ^ AND the gate has been unlocked, then do the step      

```            

This can also be chained to add in other optional steps for that environment
```
      - do-production-deploy-other-step:
          requires:
            - do-production-deploy
```

## What are the results like?

### Results:
![production branch](./production-deploy.png)

### On a normal branch - step skipped due to branch filter:
![Non production branch](./master-branch.png)
